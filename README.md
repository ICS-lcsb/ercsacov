# Paper Classification of SARS-CoV-2 Variants by Molecular Dynamics 
All code and files for the "Mechanism-based classification of SARS-CoV-2 Variants by Molecular Dynamics Resembles Phylogenetic Tree" paper.

Running title: Classification of SARS-CoV-2 Variants by Molecular Dynamics 

Affiliations

Arns, Thais1; Fouquier d’Hérouël, Aymeric1; May, Patrick1; Tkatchenko, Alexandre2; Skupin, Alexander1,2,3*.

1Luxembourg Centre for Systems Biomedicine (LCSB), University of Luxembourg, 6, avenue du Swing, L-4367, Belvaux, Luxembourg.

2Department of Physics and Material Science, University of Luxembourg, 6, avenue de la Faïencerie, L-2311, Limpertsberg, Luxembourg.

3Department of Neuroscience, University of California San Diego, 9500 Gilman Drive, La Jolla, CA 93201, USA

Corresponding author. 
Direct correspondence to alexander.skupin@uni.lu

